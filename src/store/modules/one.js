const one = {
	state: {
		count: 0,
		token: '',
	},
	mutations: {
		increment(state) {
			state.count++
		},
		setToken(state, t){
			state.token = t
		}
	},
	getters: {
		token: state => {
			return state.token
		}
	}
}

export default one
