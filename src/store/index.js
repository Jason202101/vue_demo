import Vue from 'vue'

import Vuex from 'vuex'

import one from './modules/one'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
	one: one
  }
})

export default store