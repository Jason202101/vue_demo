import Vue from "vue"

import VueRouter from 'vue-router'
import config from './modules/config'

Vue.use(VueRouter)

const routes = [
	{
		path: '/index',
		component: () => import('@/components/HelloWorld')
	},
	config
]

const Router = new VueRouter({
	routes,
	scrollBehavior: () => ({ y: 0 }),
})


export default Router
